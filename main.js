//-------------------------------------------------------------
// Game configuration data
//-------------------------------------------------------------

// This is a numerical representation of the LARVA game.
// It uses numbers to represent walls, apple,poop, empty space, and the larva.

let gameData = [

];
// Specifically, a wall is represented by the number 1,
// a coin is the number 2, empty ground is the number 3,
// and larva is the number 5,and the poop is 6.


// In code below, i want to be able to refer to names of things,
// and not numbers. To make that possible, we set up a few labels.
const WALL   = 1;
const APPLE  = 2;
const GROUND = 3;
const LARVA = 5;
const POOP   = 6;
var remainingApple = 62;
var score = 0;
var seconds = 60;
var timerDiv = document.querySelector(".time");
var temp = document.querySelector('.time');
var button = document.querySelector("button");
var sound1 = new Audio();
sound1.src= "audio/larva_eating.wav";
var sound = new Audio();
sound.src = "audio/larva_eatpoop.wav";
var timer;
var tenSecondsDeduct = 60;
var disable = true;
//i use the identifier "map" to refer to the game map.
// assign this until later on, when we generate it
// using the gameData.
let map;

// We need to keep track of larva location on the game board.
// That is done through a pair of coordinates.
// And, we will keep track of what direction she is facing.
let larva = {
  x: 6,
  y: 4,
  direction: 'right'
};

//-------------------------------------------------------------
// Game map creation functions
//-------------------------------------------------------------
// This function converts gameData into DOM elements.
function createTiles(data) {

  // We'll keep the DOM elements in an array.
  let tilesArray = [];

  // Let's take one row at a time...
  for (let row of data) {

    // Then look at each column in that row.
    for (let col of row) {

      // We create a game tile as a div element.
      let tile = document.createElement('div');

      // We assign every tile the class name tile.
      tile.classList.add('tile');

      // Now, depending on the numerical value,
      // we need to add a more specific class.
      if (col === WALL) {
        tile.classList.add('wall');

      } else if (col === APPLE) {
        tile.classList.add('apple');

      } else if (col === GROUND) {
        tile.classList.add('ground');

      }else if (col=== POOP){
        tile.classList.add('poop');
      }
       else if (col === LARVA) {
        tile.classList.add('larva');

        // For larva, we will add yet another
        // class for the direction larva is facing.
        tile.classList.add(larva.direction);

      }

      // Now that our individual tile is configured,
      // we add it to the tilesArray.
      tilesArray.push(tile);
    }

    // Once we reach the end of a row, we create a br element,
    // which tells the browser to create a line break on the page.
    let brTile = document.createElement('br');

    // We then add that br element to the tilesArray.
    tilesArray.push(brTile);
  }

  // At the end of our function, we return the array
  // of configured tiles.
  return tilesArray;
}
// This function creates a map element, fills it with tiles,
// and adds it to the page.
function drawMap() {

  map = document.createElement('div');

  let tiles = createTiles(gameData);
  for (let tile of tiles) {
    map.appendChild(tile);
  }

  document.body.appendChild(map);
}

// This function removes the map element from the page.
function eraseMap() {
  document.body.removeChild(map);
}
//-------------------------------------------------------------
// Movement functions
//-------------------------------------------------------------

// Each function does the following:
// - set larva direction so that we show the correct image
// - check to see if we hit a wall
// - if we didn't hit a wall, set larva old location to empty space
// - update larva location
// - draw larva in the new location

function moveDown() {


  larva.direction = 'down';
  if (gameData[larva.y+1][larva.x] !== WALL) {

    if (gameData[larva.y + 1][larva.x]===APPLE){
      sound1.play();
      remainingApple --;
      score=score + 10;

      console.log("Score:" + score);

      document.getElementById('score').textContent = "SCORE: "+score;


    }
    if (gameData[larva.y + 1] [larva.x]===POOP){
      sound.play();
      score=score-10;
      console.log("Score: " + score);
      document.getElementById('score').textContent = "SCORE: "+score;
    }
    gameData[larva.y][larva.x] = GROUND;
    larva.y = larva.y + 1 ;
    gameData[larva.y][larva.x] = LARVA;

  }

   if(checkApple() ==0){
     clearInterval(timer);
     document.getElementById('nextLevelBox').style.display = "block";
     //nextLevel();
    }
}
function moveUp() {
  larva.direction = 'up';
  if (gameData[larva.y-1][larva.x] !== WALL) {
    if (gameData[larva.y - 1][larva.x]===APPLE){
      sound1.play();
      remainingApple --;

      score=score + 10;
      document.getElementById('score').textContent = "SCORE: "+score;
      console.log("Score:" + score);
      document.getElementById('sound').innerHTML= "sound";


    }
    if (gameData[larva.y - 1][larva.x]===POOP){
       sound.play();
      score=score - 10;
      document.getElementById('score').textContent = "SCORE: "+score;
      console.log("Score:" + score);

    }

    gameData[larva.y][larva.x] = GROUND;
    larva.y = larva.y - 1;
    gameData[larva.y][larva.x] = LARVA;

  }
  if(checkApple() ==0){

      clearInterval(timer);
    document.getElementById('nextLevelBox').style.display = "block";
    //nextLevel();
  }
}
function moveLeft() {
  larva.direction = 'left';

  if (gameData[larva.y][larva.x-1] !== WALL) {
    if (gameData[larva.y][larva.x-1]===APPLE){
      sound1.play();
      remainingApple--;
      score=score + 10;

      document.getElementById('score').textContent = "SCORE: "+score;
      console.log("Score:" + score);



    }
    if (gameData[larva.y ][larva.x-1]===POOP){
       sound.play();
      score=score - 10;
      document.getElementById('score').textContent = "SCORE: "+score;
      console.log("Score:" + score);

    }

    gameData[larva.y][larva.x] = GROUND;
    larva.x = larva.x - 1 ;
    gameData[larva.y][larva.x] = LARVA;

  }
   if(checkApple() ==0){

       clearInterval(timer);
     document.getElementById('nextLevelBox').style.display = "block";
      //nextLevel();
    }
}
function moveRight() {
  larva.direction = 'right';
  if (gameData[larva.y][larva.x+1] !== WALL) {
    if (gameData[larva.y][larva.x+1]===APPLE){
      sound1.play();
       remainingApple --;
      score=score + 10;
      document.getElementById('score').textContent = "SCORE: "+score;
      console.log("Score:" + score);
    }
    if (gameData[larva.y][larva.x + 1 ]===POOP){
       sound.play();
      score=score - 10;
      document.getElementById('score').textContent = "SCORE: "+score;
      console.log("Score:" + score);

    }
    gameData[larva.y][larva.x] = GROUND;
    larva.x = larva.x + 1 ;
    gameData[larva.y][larva.x] = LARVA;

  }
 if(checkApple() ==0){

     clearInterval(timer);
   document.getElementById('nextLevelBox').style.display = "block";
   // nextLevel();
 }
}
function setupKeyboardControls() {

   document.addEventListener('keydown', function myFunction(e) {

    // As far as the browser is concerned, each key on the keyboard
    // is associated with a numeric value.
    // After some experimenting, you can discover which numeric values
    // correspond to the arrow keys.

    // Each time the user moves, we recalculate larva's location,
    // update the
    if (e.keyCode === 37 && disable == true) {         // left arrow is 37
      moveLeft();

    } else if (e.keyCode === 38&& disable == true) {  // up arrow is 38
      moveUp();

    } else if (e.keyCode === 39&& disable == true){   // right arrow is 39
      moveRight();

    } else if (e.keyCode === 40&& disable == true){   // down arrow is 40
      moveDown();
    }

    // After every move, we erase the map and redraw it.

    eraseMap();
    drawMap();

  });
}

function checkApple(){
  console.log("APPLE: "+remainingApple);
  if (remainingApple==0){
    return 0;
  }

}

//-------------------------------------------------------------
// Main game setup function
//-------------------------------------------------------------
function main() {

  // Initialize the game by drawing the map and setting up the
  // keyboard controls.
  drawMap();
  setupKeyboardControls();
  start ();


}


// Finally, after we define all of our functions, we need to start
// the game.
main();

function countdown() {
           timer = setInterval(function(){
           seconds --;
           temp.innerHTML = seconds;
           if (seconds === 0) {
               clearInterval(timer);
               seconds= 60;
               score.innerHTML = 0;
               remainingApple.innerHTML =0;
               gameOver();
           }
        }, 1000);
}
function nextLevel(){
  document.getElementById('nextLevelBox').style.display = "none";
  tenSecondsDeduct-=10;
  console.log(tenSecondsDeduct);
  if(tenSecondsDeduct==50){

   seconds=tenSecondsDeduct;
   clearInterval(timer);
   reInit();
  }else if(tenSecondsDeduct==40){

    seconds=tenSecondsDeduct;
  clearInterval(timer);
  reInit2();
  }else if (tenSecondsDeduct==30){

   seconds=tenSecondsDeduct;
  clearInterval(timer);
  reInit3();
  }else if(tenSecondsDeduct==20){

     seconds=tenSecondsDeduct;
  clearInterval(timer);
  reInit4();
  }else if (tenSecondsDeduct==10){

     seconds=tenSecondsDeduct;
  clearInterval(timer);
  reInit5();
  }else {

    main ();
  }
  //seconds = 60;
}
function gameOver(){
  disable = false;
   tenSecondsDeduct = 60;
   clearInterval(timer);
   document.getElementById('score').textContent = "SCORE: "+score;
   document.getElementById('gameOverBox').style.display = "block";
   document.getElementById('message').textContent = "Game over! Your score is " + score;
   score = 0;
}
function start (){
  disable = true;
  document.getElementById('gameOverBox').style.display = "none";
  document.getElementById('score').textContent = "SCORE: "+score;
  gameData = [
   [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
   [1,2,2,2,2,2,1,2,2,2,6,2,2,2,1],
   [1,2,1,1,1,2,1,2,1,1,1,2,1,2,1],
   [1,2,1,2,2,2,2,2,2,2,1,2,1,2,1],
   [1,6,2,2,1,1,5,1,1,6,2,2,6,6,1],
   [1,2,1,2,6,2,6,2,2,2,1,2,1,2,1],
   [1,2,1,1,2,2,1,2,2,1,1,2,1,2,1],
   [1,6,2,2,6,2,1,2,2,2,2,2,6,2,1],
   [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
];
 larva.x = 6;
 larva.y = 4;
 larva.direction = 'right';
 remainingApple = 54;
 eraseMap();
 drawMap();
 countdown();
 checkApple();
}
function reInit(){
  gameData = [
   [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
   [1,2,2,2,2,2,1,2,2,2,2,2,6,2,1],
   [1,2,1,1,1,2,1,2,1,1,1,2,1,2,1],
   [1,2,1,2,6,2,2,2,2,6,1,2,1,2,1],
   [1,6,2,2,1,1,5,1,1,2,2,2,1,2,1],
   [1,2,1,2,2,2,2,2,2,2,1,2,1,2,1],
   [1,2,1,1,2,2,1,2,2,1,1,2,1,2,1],
   [1,2,2,2,6,2,1,6,2,2,2,6,3,2,1],
   [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
 ];
 larva.x = 6;
 larva.y = 4;
 larva.direction = 'right';
 remainingApple= 55;
 eraseMap();
 drawMap();
 countdown();
 checkApple();
}
function reInit2(){
  gameData = [
   [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
   [1,2,2,2,2,6,1,2,2,2,2,6,2,2,1],
   [1,6,1,1,1,2,1,2,1,1,1,2,1,2,1],
   [1,2,1,2,2,2,2,2,2,2,1,2,1,2,1],
   [1,2,2,2,1,1,5,1,1,2,2,6,3,2,1],
   [1,2,1,2,2,2,2,6,2,2,1,2,1,2,1],
   [1,2,1,1,2,2,1,2,2,1,1,2,1,2,1],
   [1,2,2,6,2,2,1,2,2,6,2,2,3,2,1],
   [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
 ];
 larva.x = 6;
 larva.y = 4;
 larva.direction = 'right';
 remainingApple = 55;
 eraseMap();
 drawMap();
 countdown();
 checkApple();
}
function reInit3(){
  gameData = [
   [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
   [1,2,2,6,2,2,1,2,2,6,2,1,2,2,1],
   [1,2,1,1,1,2,1,2,1,1,1,1,1,2,1],
   [1,2,1,2,2,2,2,2,2,2,1,2,1,6,1],
   [1,2,2,2,1,1,5,1,1,2,2,2,3,2,1],
   [1,2,1,2,2,2,2,2,2,2,1,2,1,2,1],
   [1,2,1,1,2,2,1,2,6,1,1,1,1,2,1],
   [1,2,2,2,6,2,1,2,2,2,2,1,3,2,1],
   [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
 ];
 larva.x = 6;
 larva.y = 4;
 larva.direction = 'right';
 remainingApple = 53;
 eraseMap();
 drawMap();
 countdown();
 checkApple();
}
function reInit4 (){
  gameData = [
   [1,1,1,1,1,1,1,1,1,1,1,1,1],
   [1,2,2,2,2,2,1,2,2,2,2,2,1],
   [1,2,1,1,1,2,1,2,1,1,1,6,1],
   [1,6,1,2,2,2,2,2,2,2,1,2,1],
   [1,2,2,2,1,1,5,1,1,6,2,2,1],
   [1,2,1,2,2,2,2,2,2,2,1,2,1],
   [1,2,1,1,2,2,1,2,2,1,1,6,1],
   [1,2,6,2,2,2,1,2,2,2,2,2,1],
   [1,1,1,1,1,1,1,1,1,1,1,1,1]
 ];
 larva.x = 6;
 larva.y = 4;
 larva.direction = 'right';
 remainingApple = 49;
 eraseMap();
 drawMap();
 countdown();
 checkApple();
}
function reInit5(){
  gameData = [
   [1,1,1,1,1,1,1,1,1,1,1,1,1],
   [1,2,2,2,2,2,1,2,2,6,2,2,1],
   [1,2,1,1,1,2,1,2,1,1,1,6,1],
   [1,6,1,2,2,6,2,2,2,2,1,2,1],
   [1,2,2,6,1,1,5,1,1,2,2,2,1],
   [1,2,1,2,2,2,2,6,2,2,1,6,1],
   [1,2,1,1,6,2,1,2,2,1,1,2,1],
   [1,2,6,2,2,2,1,2,2,6,2,2,1],
   [1,1,1,1,1,1,1,1,1,1,1,1,1]
 ];
 larva.x = 6;
 larva.y = 4;
 larva.direction = 'right';
 remainingApple = 44;
 eraseMap();
 drawMap();
 countdown();
 checkApple();
}